package fr.lyon1.m22.potager

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.fasterxml.jackson.databind.ObjectMapper
import fr.lyon1.m22.potager.data.DataHandler
import fr.lyon1.m22.potager.demo.DummyObject
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class JsonParsingTest {

    private val appContext = InstrumentationRegistry.getTargetContext()

    @Test
    fun useAppContext() {
        // Context of the app under test.
        assertEquals("fr.lyon1.m22.potager2", appContext.packageName)
    }

    @Test
    fun testJsonParsingFromFile() {
        val test = DummyObject("test name", 12)
        val mapper = ObjectMapper()
        // file should be in src/test/resources
        val inputStream = appContext.resources.openRawResource(R.raw.test)
        val parsedJsonStringFromFile = inputStream.bufferedReader().use { it.readText() }
        val parsedJsonFromFile = mapper.readValue(parsedJsonStringFromFile, DummyObject::class.java)
        assertEquals(test, parsedJsonFromFile)

    }

    @Test
    fun testSectionParsing() {
        val parsingInputStream = appContext.resources.openRawResource(R.raw.sections)
        DataHandler.parseSections(parsingInputStream)
    }
}
