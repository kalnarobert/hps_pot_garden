package fr.lyon1.m22.potager.profile

import android.util.Log
import fr.lyon1.m22.potager.data.model.Section
import java.lang.Math.max


class ProgressTree(val sectionId: Int) {

    constructor(section: Section) : this(section.id!!) {
        subProgressTrees = Array(section.subSections?.size ?: 0,
                                 { item -> ProgressTree(section.subSections!![item]) })
        moduleProgresses = Array(section.modules?.size ?: 0,
                                 { item -> ModuleProgress(section.modules!![item]) })
        lastModuleVisited = (moduleProgresses?.getOrNull(0)?.moduleId ?: 0) - 1
    }

    var lastModuleVisited: Int? = null

    private var examScore: Int = 0
    private var subProgressTrees: Array<ProgressTree>? = null
    var moduleProgresses: Array<ModuleProgress>? = null
        private set
    val score: Int
        get() {
            val subSectionScores = subProgressTrees?.sumBy { it.score } ?: 0
            val moduleScores = moduleProgresses?.sumBy { it.score } ?: 0
            return max(examScore, subSectionScores + moduleScores)
        }
    val maxScore: Int
        get() {
            val subSectionScores = subProgressTrees?.sumBy { it.maxScore } ?: 0
            val moduleScores = moduleProgresses?.sumBy { it.maxScore } ?: 0
            return subSectionScores + moduleScores
        }
    val nextModuleId: Int?
        get() {
//            if (score > maxScore * UserProfile.THRESHOLD) {
//                return null
//            } else {
            for (progressTree in subProgressTrees.orEmpty()) {
                if (progressTree.nextModuleId != null) {
                    return progressTree.nextModuleId
                }
            }
            for (moduleIndex in 0 until moduleProgresses.orEmpty().size) {
//                    if (moduleProgresses!![moduleIndex].score < UserProfile.THRESHOLD * moduleProgresses!![moduleIndex].maxScore) {
                if (moduleProgresses!![moduleIndex].score == 0 && moduleProgresses!![moduleIndex].moduleId > lastModuleVisited ?: 0) {
                    return moduleProgresses!![moduleIndex].moduleId
                }
            }
//            }
            return null
        }
    val nextSectionId: Int?
        get() {
            Log.d("progress", "section-id: $sectionId, score: $score")
            if (score != 0) {
                return null
            } else {
                for (progressTree in subProgressTrees.orEmpty()) {
                    if (progressTree.nextModuleId != null) {
                        return progressTree.sectionId
                    }
                }
                return sectionId
            }
            return null
        }

    fun getModuleProgressById(id: Int): ModuleProgress? {
        for (progressTree in subProgressTrees.orEmpty()) {
            if (progressTree.getModuleProgressById(id) != null) {
                return progressTree.getModuleProgressById(id)
            }
        }
        for (moduleProgress in moduleProgresses.orEmpty()) {
            if (moduleProgress.moduleId == id) {
                return moduleProgress
            }
        }
        return null
    }

    fun walkTree(lambda: (ModuleProgress) -> Unit) {
        for (subProgressTree in subProgressTrees.orEmpty()) {
            subProgressTree.walkTree(lambda)
        }
        for (moduleProgress in moduleProgresses.orEmpty()) {
            lambda(moduleProgress)
        }
    }

    fun walkSections(lambda: (ProgressTree) -> Unit) {
        for (subProgressTree in subProgressTrees.orEmpty()) {
            lambda(subProgressTree)
            subProgressTree.walkSections(lambda)
        }
    }

}

