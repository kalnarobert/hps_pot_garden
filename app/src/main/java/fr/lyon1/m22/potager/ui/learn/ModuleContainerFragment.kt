package fr.lyon1.m22.potager.ui.learn

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import fr.lyon1.m22.potager.R
import fr.lyon1.m22.potager.data.DataHandler
import fr.lyon1.m22.potager.data.model.MultipleChoiceQuestion
import fr.lyon1.m22.potager.data.model.OpenQuestion
import fr.lyon1.m22.potager.data.model.ReadingMaterial
import fr.lyon1.m22.potager.ui.widgets.SubmitButton
import kotlinx.android.synthetic.main.module_layout.*
import kotlinx.android.synthetic.main.module_layout.view.*


class ModuleContainerFragment : Fragment() {

    var onAnswer: ((String) -> Unit)? = null

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.module_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        DataHandler.getModuleById(arguments?.getInt(ARGS_MODULE))?.let { module ->
            when (module) {
                is OpenQuestion -> initiateTextField()
                is MultipleChoiceQuestion -> initiateButtons(module.answers.map { it.correctVariations[0] })
                is ReadingMaterial -> initiateScrollObserver()
            }
            val path = "file://" + context!!.filesDir.path + "/"
            Log.d("web url", path)
            view.moduleDescriptionView.loadDataWithBaseURL(path,
                                                           module.question,
                                                           "text/html",
                                                           "charset=utf-8",
                                                           "UTF-8")
        }
    }

    private val readingAnswerTag = "continue"

    private fun initiateScrollObserver() {
        val layout = LayoutInflater.from(context).inflate(R.layout.answer_button, null) as ViewGroup
        val continueButton = layout.findViewById<TextView>(R.id.answer_button)
        layout.removeView(continueButton)
        continueButton.text = resources.getString(R.string.continue_course)
        continueButton.setOnClickListener({ onAnswer?.invoke(readingAnswerTag) })
        answerContainer.addView(continueButton)
    }

    private fun initiateButtons(multipleChoices: List<String>) {
        answerContainer.orientation = LinearLayout.VERTICAL
        for (choice in multipleChoices) {
            val layout = LayoutInflater.from(context).inflate(R.layout.answer_button,
                                                              null) as ViewGroup
            val choiceButton = layout.findViewById<TextView>(R.id.answer_button)
            layout.removeView(choiceButton)
            choiceButton.text = choice
            choiceButton.setOnClickListener { onAnswer?.invoke(choice) }
            answerContainer.addView(choiceButton)
        }
    }


    private fun initiateTextField() {
        var editText = EditText(context)
        editText.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT, 0.3f)
        var submit = SubmitButton(context)
        submit.layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT, 0.7f)
        submit.setOnClickListener { onAnswer?.invoke(editText.text.toString()) }

        answerContainer.orientation = LinearLayout.HORIZONTAL
        answerContainer.addView(editText)
        answerContainer.addView(submit)

    }

    companion object {

        private const val ARGS_MODULE: String = "MODULE_ARG"

        fun newInstance(moduleId: Int): ModuleContainerFragment {
            val fragment = ModuleContainerFragment()
            val args = Bundle()
            args.putInt(ARGS_MODULE, moduleId)
            fragment.arguments = args
            return fragment
        }
    }
}

