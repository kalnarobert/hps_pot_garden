package fr.lyon1.m22.potager.ui

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import fr.lyon1.m22.potager.R
import fr.lyon1.m22.potager.data.DataHandler
import fr.lyon1.m22.potager.mygarden.ui.AddPlantActivity
import fr.lyon1.m22.potager.mygarden.ui.PlantDetailActivity
import fr.lyon1.m22.potager.profile.UserProfile
import fr.lyon1.m22.potager.ui.learn.SectionActivity
import fr.lyon1.m22.potager.ui.learn.SectionListFragment
import fr.lyon1.m22.potager.ui.profile.ProfileFragment
import fr.lyon1.m22.potager.ui.simulator.SimulatorFragment
import kotlinx.android.synthetic.main.main_screen.*
import org.jsoup.Jsoup
import java.io.File
import java.io.FileOutputStream
import java.net.URLDecoder


class MainScreenActivity : AppCompatActivity(), SectionListFragment.OnSelectListener {


    companion object {
        private const val NAV_ID = "navigation_view_id"
        fun newIntent(context: Context, itemId: Int = 0): Intent {
            val intent = Intent(context, MainScreenActivity::class.java)
            intent.putExtra(NAV_ID, itemId)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            return intent
        }

        var firstRuntime: Boolean = true

        const val SECTION: String = "SECTION"
    }

    override fun onSelect(item: Int) {
        var lastModuleId = UserProfile.progressTrees[item].moduleProgresses!![0].moduleId - 1
        Log.d(SECTION, "item selected: $item,\nlastmoduleId: $lastModuleId")
        if (lastModuleId != null) {
            val moduleId = lastModuleId + 1
            Log.d(UserProfile.TAG,
                  UserProfile.progressTrees[item].getModuleProgressById(moduleId).toString())
            val module = DataHandler.getModuleById(moduleId)!!
            intent = SectionActivity.newIntent(this, module.id!!, item)
            startActivity(intent)
        } else {
            val myAlertDialog = AlertDialog.Builder(this)
            myAlertDialog.setTitle(resources.getString(R.string.title_section_dialog))
            myAlertDialog.setMessage(resources.getString(R.string.restart_section_dialog))
            myAlertDialog.setPositiveButton("OK", DialogInterface.OnClickListener { arg0, arg1 ->
                // reset section when the OK button is clicked
                UserProfile.progressTrees[item].walkTree { it.score = 0 }
                UserProfile.progressTrees[item].lastModuleVisited = UserProfile.progressTrees[item].moduleProgresses!![0].moduleId - 1
                onSelect(item)
            })
            myAlertDialog.setNegativeButton("Cancel",
                                            DialogInterface.OnClickListener { arg0, arg1 ->
                                                // don't do anything when the Cancel button is clicked
                                            })
            myAlertDialog.show()
        }
    }

    private var sectionListFragment: Fragment = SectionListFragment()
    private val simulatorFragment: Fragment = SimulatorFragment()
    private val profileFragment: Fragment = ProfileFragment()

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                supportFragmentManager.beginTransaction().replace(R.id.mainFragmentContainer,
                                                                  sectionListFragment).commit()
                mainFragmentContainer.background = getDrawable(R.drawable.hogwarts_wp)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                supportFragmentManager.beginTransaction().replace(R.id.mainFragmentContainer,
                                                                  profileFragment).commit()
                mainFragmentContainer.setBackgroundColor(Color.GRAY)
                return@OnNavigationItemSelectedListener true
            }
            R.id.simulatorMenu -> {
                supportFragmentManager.beginTransaction().replace(R.id.mainFragmentContainer,
                                                                  simulatorFragment).commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_screen)
        initializeData()
        supportFragmentManager.beginTransaction().replace(R.id.mainFragmentContainer,
                                                          sectionListFragment).commit()
        navigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        navigationView.menu.getItem(intent.extras?.getInt(NAV_ID) ?: 0).isChecked = true
        mainFragmentContainer.background = getDrawable(R.drawable.hogwarts_wp)
    }

    private fun initializeData() {
        if (firstRuntime) {
            copyFromAssets()
            val parsingInputStream = application.resources.openRawResource(R.raw.task_editor)
            DataHandler.parseSections(parsingInputStream)
            UserProfile.createNew(DataHandler.sections!!)
            firstRuntime = false
        }
    }

    private fun copyFromAssets() {
        val bufferSize = 1024
        val assetManager = assets
        val assetFiles = assetManager.list("tasks/images")

        // Documents Path
        val imageFolderName = "tasks/images"
        val imageFolder = File(filesDir, imageFolderName)
        if (!imageFolder.exists()) {
            imageFolder.mkdirs()
        }

        assetFiles.forEach {
            val inputStream = assetManager.open("tasks/images/$it")
            val outputStream = FileOutputStream(File(imageFolder, it))

            try {
                inputStream.copyTo(outputStream, bufferSize)
            } finally {
                inputStream.close()
                outputStream.flush()
                outputStream.close()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    private val TAG: String = "json_tag"

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_1 -> {
                Toast.makeText(this,
                               resources.getString(R.string.reset_profile),
                               Toast.LENGTH_SHORT).show()
                UserProfile.reset()
                true
            }
//            R.id.menu_2 -> {
//                "http://www.herriot.ovh/Exercices/task_editor.json".httpGet().responseString { request, response, result ->
//                    //do something with response
//                    when (result) {
//                        is Result.Failure -> {
//                            val ex = result.getException()
//                            Toast.makeText(this,
//                                           resources.getString(R.string.download_json_failure),
//                                           Toast.LENGTH_SHORT).show()
//                        }
//                        is Result.Success -> {
//                            Toast.makeText(this,
//                                           resources.getString(R.string.downloaded_json),
//                                           Toast.LENGTH_SHORT).show()
//                            Log.i(TAG, result.get())
//
//                            val mapper = ObjectMapper().registerKotlinModule()
//                            val actualObj = mapper.readTree(result.get())
//
//                            DataHandler.parseSections(actualObj.get("data").get("sections").toString())
//                            UserProfile.createNew(DataHandler.sections!!)
//                            sectionListFragment = SectionListFragment()
//                            supportFragmentManager.beginTransaction().replace(R.id.mainFragmentContainer,
//                                                                              sectionListFragment).commit()
//                        }
//                    }
//                }
//                MyTask(this).execute()
//
//                true
//            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun onPlantClick(view: View) {
        val imgView = view.findViewById<View>(R.id.plant_list_item_image) as ImageView
        val plantId = imgView.tag as Long
        val intent = Intent(this, PlantDetailActivity::class.java)
        intent.putExtra(PlantDetailActivity.EXTRA_PLANT_ID, plantId)
        startActivity(intent)
    }

    fun onAddFabClick(view: View) {
        val intent = Intent(this, AddPlantActivity::class.java)
        startActivity(intent)
    }

    class MyTask(val context: Context) : AsyncTask<Unit, Unit, Unit>() {
        override fun doInBackground(vararg params: Unit?) {
            val doc = Jsoup.connect("http://www.herriot.ovh/Exercices/images").get()
            for (file in doc.select("td > a")) {
                if (file.attr("href") != "/Exercices/") {
                    Log.i("jsoup", file.attr("href"))
                    val fileName = URLDecoder.decode(file.attr("href"), "UTF-8");
                    val imageFolder = File(context.filesDir, "tasks/images/")
                    Fuel.download("http://www.herriot.ovh/Exercices/images/${file.attr("href")}").destination { response, url ->
                        File(imageFolder, fileName)
                    }.response { req, res, result ->

                    }
                    Log.i("img", imageFolder.listFiles().joinToString(", "))
                }
            }
        }

    }

}
