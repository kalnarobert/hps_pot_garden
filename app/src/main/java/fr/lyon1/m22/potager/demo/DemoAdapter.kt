package fr.lyon1.m22.potager.demo

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.lyon1.m22.potager.R
import kotlinx.android.synthetic.main.demo_recycle_layout.view.*

class DemoAdapter(private val myDataset: Array<DummyObject>) : RecyclerView.Adapter<DemoAdapter.ViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DemoAdapter.ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context).inflate(R.layout.demo_recycle_layout,
                parent,
                false)
        // set the view's size, margins, paddings and layout parameters ...
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.view.demo_text.text = myDataset[position].name
        holder.view.demo_id.text = myDataset[position].age.toString()
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}


