package fr.lyon1.m22.potager.data

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import fr.lyon1.m22.potager.data.model.Module
import fr.lyon1.m22.potager.data.model.Section
import java.io.InputStream

class DataHandler {
    companion object {

        var sections: Array<Section>? = null
        fun parseSections(inputStream: InputStream) {
            if (sections == null) {
                val parsedJsonStringFromFile = inputStream.bufferedReader().use { it.readText() }
                parseSections(parsedJsonStringFromFile)
            }
        }
        fun parseSections(json: String) {
            val mapper = ObjectMapper().registerKotlinModule()
            mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true)
            sections = mapper.readValue(json, Array<Section>::class.java)
        }

        fun getModuleById(id: Int?): Module? {
            id?.let {
                for (section in sections.orEmpty()) {
                    if (section.getModuleById(it) != null) {
                        return section.getModuleById(it)
                    }
                }
                return null
            } ?: run {
                return null
            }
        }

        fun listModulesOfSectionById(int: Int): ArrayList<String> {
            return sectionById(int)!!.listModules()
        }

        fun sectionById(sectionId: Int): Section? {
            for (section in sections.orEmpty()) {
                if (section.id == sectionId) {
                    return section
                }
                val foundSection = section.getSectionById(sectionId)
                if (foundSection != null) {
                    return foundSection
                }
            }
            return null
        }


    }
}

private fun Section.getSectionById(sectionId: Int): Section? {
    for (subSection in subSections.orEmpty()) {
        if (subSection.id == sectionId) {
            return subSection
        }
        if (subSection.getSectionById(sectionId) != null) {
            return subSection.getSectionById(sectionId)
        }
    }
    return null
}

private fun Section.listModules(): ArrayList<String> {
    var moduleList = ArrayList<String>()
    for (section in subSections.orEmpty()) {
        moduleList.addAll(section.listModules())
    }
    for (module in modules.orEmpty()) {
        moduleList.add(module.name ?: "module #${module.id}")
    }
    return moduleList
}

private fun Section.getModuleById(id: Int): Module? {
    if (exam?.id == id) {
        return exam
    }
    for (section in subSections.orEmpty()) {
        if (section.getModuleById(id) != null) {
            return section.getModuleById(id)
        }
    }
    for (module in modules.orEmpty()) {
        if (module.id == id) {
            return module
        }
    }
    return null
}
