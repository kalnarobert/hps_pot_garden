package fr.lyon1.m22.potager.profile

import fr.lyon1.m22.potager.data.model.Module

class ModuleProgress(var score: Int = 0, val moduleId: Int, val maxScore: Int) {
    var hint: String? = null

    constructor(module: Module) : this(0, module.id!!, module.maxScore)

    override fun toString(): String {
        return "ModuleProgress(score=$score, moduleId=$moduleId, maxScore=$maxScore)"
    }


}
