package fr.lyon1.m22.potager.ui.learn

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kofigyan.stateprogressbar.StateProgressBar
import fr.lyon1.m22.potager.R
import fr.lyon1.m22.potager.data.DataHandler
import kotlinx.android.synthetic.main.state_progress_contianer.*

class SectionProgressContainerFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.state_progress_contianer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val descriptionData: List<String> = DataHandler.listModulesOfSectionById(arguments?.getInt(
                ARGS_MODULE)!!).map { it.replace(" ", "\n") }
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE)
        Log.d("progressContainer", "${stateProgressBar.currentStateNumber}")
        stateProgressBar.setMaxStateNumber(descriptionData.size)
        stateProgressBar.setStateDescriptionData(descriptionData.toTypedArray())
    }

    companion object {

        private const val ARGS_MODULE: String = "SECTION_ID"

        fun newInstance(sectionId: Int): SectionProgressContainerFragment {
            val fragment = SectionProgressContainerFragment()
            val args = Bundle()
            args.putInt(ARGS_MODULE, sectionId)
            fragment.arguments = args
            return fragment
        }
    }
}


private fun StateProgressBar.setMaxStateNumber(size: Int) {
    val stateNumber = when (size) {
        1 -> StateProgressBar.StateNumber.ONE
        2 -> StateProgressBar.StateNumber.TWO
        3 -> StateProgressBar.StateNumber.THREE
        4 -> StateProgressBar.StateNumber.FOUR
        5 -> StateProgressBar.StateNumber.FIVE
        else -> StateProgressBar.StateNumber.ONE
    }
    setMaxStateNumber(stateNumber)
}
