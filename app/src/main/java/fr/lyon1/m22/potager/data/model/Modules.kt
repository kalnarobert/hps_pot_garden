package fr.lyon1.m22.potager.data.model

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.util.*

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes(JsonSubTypes.Type(OpenQuestion::class, name = "open question"),
              JsonSubTypes.Type(MultipleChoiceQuestion::class, name = "multiple choice question"),
              JsonSubTypes.Type(ReadingMaterial::class, name = "reading"))
abstract class Module(
        open var id: Int?,
        open val name: String?,
        open val description: String?,
        open val question: String?,
        open val maxScore: Int,
        open val answers: Array<Answer>,
        open var sectionId: Int
) {

    init {
        counter++
    }

    companion object {
        var counter: Int = 0
    }
}

data class OpenQuestion(
        override var id: Int?,
        override val name: String?,
        override val description: String?,
        override val question: String?,
        override val maxScore: Int,
        override val answers: Array<Answer>,
        override var sectionId: Int
) : Module(id, name, description, question, maxScore, answers, sectionId) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OpenQuestion

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (question != other.question) return false
        if (maxScore != other.maxScore) return false
        if (!Arrays.equals(answers, other.answers)) return false
        if (sectionId != other.sectionId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (question?.hashCode() ?: 0)
        result = 31 * result + maxScore
        result = 31 * result + Arrays.hashCode(answers)
        result = 31 * result + sectionId
        return result
    }

    init {
        id = counter
    }

}

data class ReadingMaterial(
        override var id: Int?,
        override val name: String?,
        override val description: String?,
        override val question: String?,
        override val maxScore: Int,
        override val answers: Array<Answer>,
        override var sectionId: Int = 0
) : Module(id, name, description, question, maxScore, answers, sectionId) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MultipleChoiceQuestion

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (question != other.question) return false
        if (maxScore != other.maxScore) return false
        if (!Arrays.equals(answers, other.answers)) return false
        if (sectionId != other.sectionId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (question?.hashCode() ?: 0)
        result = 31 * result + maxScore
        result = 31 * result + Arrays.hashCode(answers)
        result = 31 * result + sectionId
        return result
    }

    init {
        id = counter
    }
}

data class MultipleChoiceQuestion(
        override var id: Int?,
        override val name: String?,
        override val description: String?,
        override val question: String?,
        override val maxScore: Int,
        override val answers: Array<Answer>,
        override var sectionId: Int = 0
) : Module(id, name, description, question, maxScore, answers, sectionId) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MultipleChoiceQuestion

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (question != other.question) return false
        if (maxScore != other.maxScore) return false
        if (!Arrays.equals(answers, other.answers)) return false
        if (sectionId != other.sectionId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (question?.hashCode() ?: 0)
        result = 31 * result + maxScore
        result = 31 * result + Arrays.hashCode(answers)
        result = 31 * result + sectionId
        return result
    }

    init {
        id = counter
    }
}

data class Answer(
        val correctVariations: Array<String>,
        val score: Int = 0,
        val typingErrorLimit: Int = 0,
        val hint: String?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Answer

        if (!Arrays.equals(correctVariations, other.correctVariations)) return false
        if (score != other.score) return false

        return true
    }

    override fun hashCode(): Int {
        var result = Arrays.hashCode(correctVariations)
        result = 31 * result + score
        return result
    }
}
