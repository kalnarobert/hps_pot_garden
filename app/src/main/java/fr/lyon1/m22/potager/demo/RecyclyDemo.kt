package fr.lyon1.m22.potager.demo

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import fr.lyon1.m22.potager.R

class RecyclyDemo : Activity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.recycly_demo)

        viewManager = LinearLayoutManager(this)
        viewAdapter = DemoAdapter(createDemoData())

        recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }
    }

    private fun createDemoData(): Array<DummyObject> {
        return Array(30, { item -> DummyObject("test $item", item) })
    }
}

data class DummyObject(val name: String = "", val age: Int = 0)
