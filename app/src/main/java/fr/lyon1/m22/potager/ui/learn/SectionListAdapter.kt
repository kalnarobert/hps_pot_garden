package fr.lyon1.m22.potager.ui.learn

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import fr.lyon1.m22.potager.R
import fr.lyon1.m22.potager.data.model.Section


class SectionListAdapter(
        private val sections: Array<Section>,
        private val onSelectListener: SectionListFragment.OnSelectListener
) : RecyclerView.Adapter<SectionListAdapter.SectionViewHolder>() {

    override fun onBindViewHolder(holder: SectionViewHolder, position: Int) {
        holder.iconView.text = sections[position].name
        holder.descriptionView.text = sections[position].description
        holder.view.setOnClickListener({ _ -> onSelectListener.onSelect(position) })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.section_item_fragment,
                parent,
                false)
        return SectionViewHolder(view)
    }

    override fun getItemCount(): Int = sections.size

    inner class SectionViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val iconView: TextView = view.findViewById(R.id.section_icon)
        val descriptionView: TextView = view.findViewById(R.id.section_description)
        lateinit var section: Section
    }


}
