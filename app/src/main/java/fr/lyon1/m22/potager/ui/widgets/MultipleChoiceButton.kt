package fr.lyon1.m22.potager.ui.widgets

import android.content.Context
import android.view.ViewGroup
import android.widget.Button

class MultipleChoiceButton : Button {

    constructor(context: Context?, choice: String) : super(context) {
        text = choice
        width = ViewGroup.LayoutParams.MATCH_PARENT
    }
}
