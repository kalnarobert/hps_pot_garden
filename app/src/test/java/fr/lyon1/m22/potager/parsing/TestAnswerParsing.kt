package fr.lyon1.m22.potager.parsing

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import fr.lyon1.m22.potager.data.model.Answer
import fr.lyon1.m22.potager.logic.ModuleGrader
import org.junit.Assert.assertEquals
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TestAnswerParsing {

    private fun <T> parseSections(filename: String, java: Class<T>): T {
        val mapper = ObjectMapper().registerKotlinModule()
        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true)
        // file should be in src/test/resources
        val inputStream = javaClass.classLoader.getResourceAsStream(filename)
        val parsedJsonStringFromFile = inputStream.bufferedReader().use { it.readText() }
        return mapper.readValue(parsedJsonStringFromFile, java)
    }

    @Test
    fun testJsonParsingFromString() {
        var answers = parseSections("answers.json", Array<Answer>::class.java)
        assertEquals(10, answers[0].score)
        assertEquals(5, answers[0].typingErrorLimit)
        assertEquals(10, ModuleGrader.validateAnswer("correct", answers))
    }

    @Test
    fun testHammingDistance() {
        assertEquals(2, ModuleGrader.Hamming.compute("asd", "asdfg"))
        assertEquals(0, ModuleGrader.Hamming.compute("asd", "asd"))
        assertEquals(3, ModuleGrader.Hamming.compute("1ahsd", "aaisd9"))
    }

}
