package fr.lyon1.m22.potager.parsing

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import fr.lyon1.m22.potager.data.model.Module
import fr.lyon1.m22.potager.data.model.MultipleChoiceQuestion
import fr.lyon1.m22.potager.data.model.OpenQuestion
import fr.lyon1.m22.potager.data.model.ReadingMaterial
import fr.lyon1.m22.potager.data.model.Section
import fr.lyon1.m22.potager.profile.UserProfile
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TestModuleAndSectionParsing {

    @Before
    fun resetCounters() {
        Module.counter = 0
    }

    private fun parseSections(filename: String): Array<Section> {
        val mapper = ObjectMapper().registerKotlinModule()
        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true)
        // file should be in src/test/resources
        val inputStream = javaClass.classLoader.getResourceAsStream(filename)
        val parsedJsonStringFromFile = inputStream.bufferedReader().use { it.readText() }
        return mapper.readValue(parsedJsonStringFromFile, Array<Section>::class.java)
    }

    @Test
    fun testModuleParsing() {
        val sections = parseSections("sections_profile_test.json")
        val id1 = sections[0].subSections!![0].modules!![0].id
        val id2 = sections[0].modules!![0].id
        assertEquals(1, id1)
        assertEquals(2, id2)
    }


    @Test
    fun testTypeParsing() {
        val sections = parseSections("sections_test_types.json")

        assertTrue(sections[0].exam is OpenQuestion)
        assertTrue(sections[0].subSections!![0].modules!![0] is MultipleChoiceQuestion)
        assertTrue(sections[0].modules!![0] is ReadingMaterial)

    }

    @Test
    fun testMaxScore() {
        val sections = parseSections("testProfileMaxScore.json")
        UserProfile.createNew(sections)

        assertEquals(19, UserProfile.progressTrees.sumBy { it.maxScore })
    }

    @Test
    fun testSectionParsing() {
        val mapper = ObjectMapper().registerKotlinModule()
        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true)
        // file should be in src/test/resources
        val inputStream = javaClass.classLoader.getResourceAsStream("testSections.json")
        val parsedJsonStringFromFile = inputStream.bufferedReader().use { it.readText() }
        val sections = mapper.readValue(parsedJsonStringFromFile, Array<Section>::class.java)
        assertTrue(sections[0].subSections!![0].modules!![0].answers[0].correctVariations.contains("correct"))
    }

    @Test
    fun testNullArray() {
        var nullArray: Array<Int>? = null
        var testNewArrayBuiltFromNullArray = Array<Int>(nullArray?.size ?: 0,
                { item -> nullArray!![item] })

        print(testNewArrayBuiltFromNullArray.size)

    }
}

