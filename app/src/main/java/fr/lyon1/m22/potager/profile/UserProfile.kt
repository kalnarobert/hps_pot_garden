package fr.lyon1.m22.potager.profile

import android.util.Log
import fr.lyon1.m22.potager.data.model.Section
import kotlin.math.min

class UserProfile {

    companion object {
        const val THRESHOLD = 0.0
        lateinit var progressTrees: Array<ProgressTree>

        var simulatorScore: Float = 0f
            set(value) {
                field = min(value, 10f)
            }

        fun addSimulatorScore(diff: Float) {
            simulatorScore = simulatorScore.plus(diff)
        }

        fun createNew(sections: Array<Section>) {
            progressTrees = Array(sections.size, { item -> ProgressTree(sections[item]) })
        }

        fun nextModule(): Int? {
            for (progressTree in progressTrees) {
                if (progressTree.sectionId == lastVisitedSectionId) {
                    if (progressTree.nextModuleId != null) {
                        return progressTree.nextModuleId!!
                    }
                }
            }
            return null
        }

        var lastModuleVisitedId: Int = 0
        var lastVisitedSectionId: Int = 0

        fun loadProfile() {}
        const val TAG: String = "USER_PROFILE"

        fun onFinishedModule(obtainedScore: Int) {
            progressTrees.forEach { progressTree ->
                run {
                    progressTree.getModuleProgressById(lastModuleVisitedId)?.let {
                        it.score = obtainedScore
                        Log.d(UserProfile.TAG, it.toString())
                    }
                }
            }
        }

        fun reset() {
            walkModuleProgress({ moduleProgress -> moduleProgress.score = 0 })
            simulatorScore = 0f
        }

        fun walkModuleProgress(lambda: (ModuleProgress) -> Unit) {
            for (progressTree in progressTrees) {
                progressTree.walkTree(lambda)
            }
        }

        fun walkSectionProgresses(lambda: (ProgressTree) -> Unit) {
            for (progressTree in progressTrees) {
                lambda(progressTree)
                progressTree.walkSections(lambda)
            }
        }

        fun nextSectionId(): Int? {
            for (progressTree in progressTrees) {
                if (progressTree.nextSectionId != null) {
                    return progressTree.nextSectionId!!
                }
            }
            return null
        }

        fun updateHint(hint: String) {
            progressTrees.forEach { progressTree ->
                run {
                    progressTree.getModuleProgressById(lastModuleVisitedId)?.let {
                        it.hint = hint
                        Log.d(UserProfile.TAG, it.toString())
                    }
                }
            }
        }

    }


}

