package fr.lyon1.m22.potager.ui.widgets

import android.content.Context
import android.widget.Button
import fr.lyon1.m22.potager.R

class SubmitButton : Button {

    constructor(context: Context?) : super(context) {
        if (context != null) {
            text = context.resources.getString(R.string.validate_button)
        }
    }

}
