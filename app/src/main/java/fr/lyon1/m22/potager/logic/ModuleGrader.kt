package fr.lyon1.m22.potager.logic

import android.util.Log
import fr.lyon1.m22.potager.data.DataHandler
import fr.lyon1.m22.potager.data.model.Answer
import fr.lyon1.m22.potager.profile.UserProfile
import java.lang.Math.abs
import java.lang.Math.max

class ModuleGrader {

    companion object {
        private const val VALIDATION: String = "module grader"

        fun validateAnswer(answerString: String): Int? {


            val module = DataHandler.getModuleById(UserProfile.lastModuleVisitedId)
            val score = validateAnswer(answerString, module?.answers.orEmpty() as Array<Answer>)
            return score
        }

        fun validateAnswer(answerString: String, answers: Array<Answer>): Int? {
            var score = 0
            for (answer in answers) {
                val tempScore = score
                Log.d(VALIDATION,
                      "your answer: $answerString,  answer to be tested against: ${answer.correctVariations[0]}")
                score = max(score, compareAnswer(answerString, answer))
            }
            return score
        }

        private fun compareAnswer(answerString: String, answer: Answer): Int {
            return if (answer.correctVariations.map { it.toLowerCase() }.count {
                        Hamming.compute(answerString.toLowerCase(), it) <= answer.typingErrorLimit
                    } > 0) {
                if (answer.hint != null) {
                    UserProfile.updateHint(answer.hint)
                }
                answer.score
            } else {
                0
            }
        }
    }


    object Hamming {
        fun compute(strand1: String, strand2: String): Int {
            return strand1.toCharArray().zip(strand2.toCharArray()).sumBy {
                if (it.first == it.second) 0 else 1
            } + abs(strand1.length - strand2.length)
        }
    }
}
