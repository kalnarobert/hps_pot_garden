package fr.lyon1.m22.potager.ui.profile

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import fr.lyon1.m22.potager.R
import fr.lyon1.m22.potager.data.DataHandler
import fr.lyon1.m22.potager.profile.UserProfile
import kotlinx.android.synthetic.main.profile_layout.*


class ProfileFragment : Fragment() {


    val TOUCH_TAG = "touch"


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.profile_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        globalScoreView.text = "${UserProfile.progressTrees.sumBy { it.score } + UserProfile.simulatorScore} / ${UserProfile.progressTrees.sumBy { it.maxScore } + 10}"
        simulatorScoreView.text = "${UserProfile.simulatorScore} / 10"

        UserProfile.walkSectionProgresses { sectionProgress ->
            val moduleMaxScore = sectionProgress.moduleProgresses.orEmpty().sumBy { it.maxScore }
            if (moduleMaxScore != 0) {
                // title

                val textView = TextView(context)
                val section = DataHandler.sectionById(sectionProgress.sectionId)!!
                textView.text = section.name ?: "Section ${sectionProgress.sectionId}"
                val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                                       LinearLayout.LayoutParams.WRAP_CONTENT)
                params.setMargins(5, 50, 5, 5)
                textView.layoutParams = params
                val face = Typeface.createFromAsset(context!!.assets, "fonts/harry_p.otf")
                textView.typeface = face
                chartContainer.addView(textView)

                // chart

                val barChart = BarChart(context)
                val maxScoreEntries = Array(section.modules.orEmpty().size, {
                    BarEntry(it.toFloat(), section.modules!![it].maxScore.toFloat())
                })
                val maxScoreDataSet = BarDataSet(maxScoreEntries.toMutableList(),
                                                 resources.getString(R.string.max_score))

                val scoreEntries = Array(section.modules.orEmpty().size, {
                    BarEntry(it.toFloat(), sectionProgress.moduleProgresses!![it].score.toFloat())
                })

                val scoreDataSet = BarDataSet(scoreEntries.toMutableList(),
                                              resources.getString(R.string.profile_score_label))
                scoreDataSet.color = Color.rgb(44, 66, 33)

                val barData = BarData(maxScoreDataSet, scoreDataSet)

                barChart.data = barData
                barChart.description = null

                barChart.setDrawGridBackground(false)
                barChart.setDrawBorders(false)

                var leftAxis = barChart.axisLeft
                leftAxis.setDrawGridLines(false)
                leftAxis.axisMinimum = 0f

                barChart.axisRight.isEnabled = false
                val xAxis = barChart.xAxis
                xAxis.position = XAxisPosition.BOTTOM
                xAxis.granularity = 1f
                xAxis.valueFormatter = IAxisValueFormatter { index, _ -> section.modules!![index.toInt()].name }
                xAxis.textSize = 8f
                barChart.invalidate()
                val chartHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                                                            150f,
                                                            resources.displayMetrics)
                barChart.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                                  chartHeight.toInt())
                barChart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
                    override fun onNothingSelected() {
                    }

                    override fun onValueSelected(e: Entry?, h: Highlight?) {
                        if (e != null) {
                            Log.d(TOUCH_TAG, "entry: ${e.y}")
                            val hint = sectionProgress.moduleProgresses!![e.x.toInt()].hint
                            if (hint != null) {
                                Log.d(TOUCH_TAG, hint)
                                val myAlertDialog = AlertDialog.Builder(context!!)
                                myAlertDialog.setTitle(resources.getString(R.string.title_hint_dialog))
                                myAlertDialog.setMessage(hint)
                                myAlertDialog.setPositiveButton("OK",
                                                                DialogInterface.OnClickListener { arg0, arg1 ->
                                                                    // reset section when the OK button is clicked
                                                                })
                                myAlertDialog.show()
                            } else {
                                Log.d(TOUCH_TAG, "no hint")
                            }
                        }
                    }

                })

                chartContainer.addView(barChart)
            }

        }

    }
}