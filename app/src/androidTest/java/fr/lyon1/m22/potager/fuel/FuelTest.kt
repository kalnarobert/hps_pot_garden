package fr.lyon1.m22.potager.fuel

import android.util.Log
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import junit.framework.Assert.assertNotNull
import org.jsoup.Jsoup
import org.junit.Test
import java.io.File


class FuelTest {

    @Test
    fun testFuelBlockingMode() {
        val (request, response, result) = "http://kalnar.eu/assets/logiciels-educatifs/sections.json".httpGet().responseString() // result is Result<String, FuelError>

        println(response.responseMessage)

        println(result.get())

    }

    @Test
    fun testJsonDownload() {
        "http://www.herriot.ovh/Exercices/task_editor.json".httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                }
                is Result.Success -> {
                    Log.i("test", result.get())

                    val mapper = ObjectMapper().registerKotlinModule()
                    val actualObj = mapper.readTree(result.get())
                    assertNotNull(actualObj)
                    Log.i("extracted", actualObj.toString())

                    Log.i("extracted", actualObj.get("data").get("sections").toString())

                }
            }
        }
    }

    @Test
    fun listImgFiles() {

//        "http://www.herriot.ovh/Exercices/images".httpGet().responseString { request, response, result ->
//            when (result) {
//                is Result.Failure -> {
//                    val ex = result.getException()
//                }
//                is Result.Success -> {
//                    Log.i("test", result.get())
//
//                }
//            }
//        }

//        val doc = Jsoup.connect("http://www.herriot.ovh/Exercices/images").get()
//        for (file in doc.select("td > a")) {
//            if (file.attr("href") != "/Exercices/") {
//                Log.i("jsoup", file.attr("href"))
//                Fuel.download("http://www.herriot.ovh/Exercices/images/${file.attr("href")}").destination { response, url ->
//                    File.createTempFile("temp", ".tmp")
//                    File(filesDir, "test.txt")
//                }.response { req, res, result ->
//
//                }
//            }
//        }


    }
}