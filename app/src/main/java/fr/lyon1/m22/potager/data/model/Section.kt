package fr.lyon1.m22.potager.data.model

import java.util.*


data class Section(
        val id: Int?,
        val name: String?,
        val description: String?,
        val exam: Module?,
        val subSections: Array<Section>?,
        val modules: Array<Module>?,
        val random: Boolean = false
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Section

        if (name != other.name) return false
        if (description != other.description) return false
        if (exam != other.exam) return false
        if (!Arrays.equals(subSections, other.subSections)) return false
        if (!Arrays.equals(modules, other.modules)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = (name ?: "").hashCode()
        result = 31 * result + (description ?: "").hashCode()
        result = 31 * result + (exam?.hashCode() ?: 0)
        result = 31 * result + Arrays.hashCode(subSections)
        result = 31 * result + Arrays.hashCode(modules)
        return result
    }
}


