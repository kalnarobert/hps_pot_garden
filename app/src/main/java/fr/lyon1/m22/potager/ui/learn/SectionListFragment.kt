package fr.lyon1.m22.potager.ui.learn

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.lyon1.m22.potager.R
import fr.lyon1.m22.potager.data.DataHandler


class SectionListFragment : Fragment() {

    private lateinit var onSelectListener: OnSelectListener

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view: RecyclerView = inflater.inflate(R.layout.fragment_section_list,
                container,
                false) as RecyclerView
        view.layoutManager = LinearLayoutManager(view.context)
        view.adapter = SectionListAdapter(DataHandler.sections!!, onSelectListener)
        return view
    }

    interface OnSelectListener {
        // TODO: Update argument type and name
        fun onSelect(item: Int)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnSelectListener) {
            onSelectListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }

    }
}