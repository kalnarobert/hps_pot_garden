package fr.lyon1.m22.potager.mygarden.ui;

/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.lyon1.m22.potager.R;
import fr.lyon1.m22.potager.mygarden.PlantWateringService;
import fr.lyon1.m22.potager.mygarden.provider.PlantContract;
import fr.lyon1.m22.potager.mygarden.utils.PlantUtils;
import fr.lyon1.m22.potager.profile.UserProfile;

import static fr.lyon1.m22.potager.mygarden.provider.PlantContract.BASE_CONTENT_URI;
import static fr.lyon1.m22.potager.mygarden.provider.PlantContract.PATH_PLANTS;
import static fr.lyon1.m22.potager.mygarden.utils.PlantUtils.DANGER_AGE_WITHOUT_WATER;
import static fr.lyon1.m22.potager.mygarden.utils.PlantUtils.FULLY_GROWN_AGE;
import static fr.lyon1.m22.potager.mygarden.utils.PlantUtils.MAX_AGE_WITHOUT_WATER;

public class PlantDetailActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int SINGLE_LOADER_ID = 200;
    public static final String EXTRA_PLANT_ID = "fr.lyon1.m22.potager.mygarden.extra.PLANT_ID";
    long mPlantId;

    Handler h = new Handler();
    int delay = 1000; //1 second=1000 milisecond, 15*1000=15seconds
    Runnable runnable;

    long waterAge = 0;
    long plantAge = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_detail);
        mPlantId = getIntent().getLongExtra(EXTRA_PLANT_ID, PlantContract.INVALID_PLANT_ID);
        // This activity displays single plant information that is loaded using a cursor loader
        getSupportLoaderManager().initLoader(SINGLE_LOADER_ID, null, this);
    }

    public void onBackButtonClick(View view) {
        finish();
    }

    public void onWaterButtonClick(View view) {
        h.removeCallbacks(runnable); //stop handler when activity not visible
        PlantWateringService.startActionWaterPlant(this, mPlantId);
//        getLoaderManager().getLoader(SINGLE_LOADER_ID).reset();
        getSupportLoaderManager().restartLoader(SINGLE_LOADER_ID, null, this);
//        h.postDelayed(new UpdateUi(createdAt, wateredAt, plantType), delay);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri SINGLE_PLANT_URI = ContentUris.withAppendedId(
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PLANTS).build(), mPlantId);
        return new CursorLoader(this, SINGLE_PLANT_URI, null,
                null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null || cursor.getCount() < 1) return;
        cursor.moveToFirst();
        //do something
        int createTimeIndex = cursor.getColumnIndex(PlantContract.PlantEntry.COLUMN_CREATION_TIME);
        int waterTimeIndex = cursor.getColumnIndex(PlantContract.PlantEntry.COLUMN_LAST_WATERED_TIME);
        int planTypeIndex = cursor.getColumnIndex(PlantContract.PlantEntry.COLUMN_PLANT_TYPE);

        int plantType = cursor.getInt(planTypeIndex);
        long createdAt = cursor.getLong(createTimeIndex);
        long wateredAt = cursor.getLong(waterTimeIndex);
        h.removeCallbacks(runnable); //stop handler when activity not visible
        runnable = new UpdateUi(createdAt, wateredAt, plantType);
        h.postDelayed(runnable, delay);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    protected void onPause() {
        h.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }


    public void onCutButtonClick(View view) {
        Uri SINGLE_PLANT_URI = ContentUris.withAppendedId(
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PLANTS).build(), mPlantId);
        getContentResolver().delete(SINGLE_PLANT_URI, null, null);
        PlantWateringService.startActionUpdatePlantWidgets(this);
        finish();
    }

    public void onMagicButtonClick(View view) {
        Log.d("GARDEN", "magic!");
    }

    public void onHarvestButtonClick(View view) {
        PlantUtils.PlantStatus status = PlantUtils.PlantStatus.ALIVE;
        if (waterAge > MAX_AGE_WITHOUT_WATER) status = PlantUtils.PlantStatus.DEAD;
        else if (waterAge > DANGER_AGE_WITHOUT_WATER) status = PlantUtils.PlantStatus.DYING;

        //Update image if old enough
        if (plantAge > FULLY_GROWN_AGE) {
            switch (status) {
                case DEAD:
                    UserProfile.Companion.addSimulatorScore(-1f);
                    break;
                case ALIVE:
                    UserProfile.Companion.addSimulatorScore(1f);
                    break;
                case DYING:
                    UserProfile.Companion.addSimulatorScore(-0.5f);
                    break;
            }
        } else {
            switch (status) {
                case DEAD:
                    UserProfile.Companion.addSimulatorScore(-1f);
                    break;
                case ALIVE:
                    UserProfile.Companion.addSimulatorScore(-.25f);
                    break;
                case DYING:
                    UserProfile.Companion.addSimulatorScore(-0.5f);
                    break;
            }
        }
    }

    class UpdateUi implements Runnable {

        public UpdateUi(long createdAt, long wateredAt, int plantType) {
            this.createdAt = createdAt;
            this.wateredAt = wateredAt;
            this.plantType = plantType;
        }

        private long createdAt;
        private long wateredAt;
        private int plantType;

        public void run() {
            long timeNow = System.currentTimeMillis();

            waterAge = timeNow - wateredAt;
            plantAge = timeNow - createdAt;
            int plantImgRes = PlantUtils.getPlantImageRes(getBaseContext(), timeNow - createdAt, timeNow - wateredAt, plantType);

            ((ImageView) findViewById(R.id.plant_detail_image)).setImageResource(plantImgRes);
            ((TextView) findViewById(R.id.plant_detail_name)).setText(String.valueOf(mPlantId));
            ((TextView) findViewById(R.id.plant_age_number)).setText(
                    String.valueOf(PlantUtils.getDisplayAgeInt(timeNow - createdAt))
            );
            ((TextView) findViewById(R.id.plant_age_unit)).setText(
                    PlantUtils.getDisplayAgeUnit(getBaseContext(), timeNow - createdAt)
            );
            ((TextView) findViewById(R.id.last_watered_number)).setText(
                    String.valueOf(PlantUtils.getDisplayAgeInt(timeNow - wateredAt))
            );
            ((TextView) findViewById(R.id.last_watered_unit)).setText(
                    PlantUtils.getDisplayAgeUnit(getBaseContext(), timeNow - wateredAt)
            );
            int waterPercent = 100 - ((int) (100 * (timeNow - wateredAt) / MAX_AGE_WITHOUT_WATER));
            ((WaterLevelView) findViewById(R.id.water_level)).setValue(waterPercent);
            //start handler as activity become visible


            h.postDelayed(this, delay);
        }
    }
}
