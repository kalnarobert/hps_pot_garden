package fr.lyon1.m22.potager.ui.simulator

import android.database.Cursor
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.CursorLoader
import android.support.v4.content.Loader
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.lyon1.m22.potager.R
import fr.lyon1.m22.potager.mygarden.provider.PlantContract
import fr.lyon1.m22.potager.mygarden.provider.PlantContract.BASE_CONTENT_URI
import fr.lyon1.m22.potager.mygarden.provider.PlantContract.PATH_PLANTS
import fr.lyon1.m22.potager.mygarden.ui.PlantListAdapter
import fr.lyon1.m22.potager.mygarden.ui.PlantListAdapter.GARDEN_TAG


class SimulatorFragment : Fragment(), LoaderManager.LoaderCallbacks<Cursor> {

    private val GARDEN_LOADER_ID = 100
    private var mAdapter: PlantListAdapter? = null

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        val PLANT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_PLANTS).build()
        return CursorLoader(requireContext(),
                            PLANT_URI,
                            null,
                            null,
                            null,
                            PlantContract.PlantEntry.COLUMN_CREATION_TIME)
    }

    override fun onLoadFinished(loader: Loader<Cursor>, cursor: Cursor) {
        cursor.moveToFirst()
        Log.d(GARDEN_TAG, "onLoadFinished: cursor state: " + cursor.toString())
        mAdapter?.swapCursor(cursor)
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        mAdapter?.swapCursor(null)
    }


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_main, container, false)
        // The main activity displays the garden as a grid layout recycler view
        val gardenRecycleView: RecyclerView = view.findViewById(R.id.gardenRecycleView)
        gardenRecycleView.layoutManager = GridLayoutManager(context,
                                                            4) as RecyclerView.LayoutManager?
        mAdapter = PlantListAdapter(context, null)
        gardenRecycleView.adapter = mAdapter


        if (loaderManager.getLoader<Cursor>(GARDEN_LOADER_ID) != null) {
            loaderManager.initLoader(GARDEN_LOADER_ID, null, this)
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        if (loaderManager.getLoader<Cursor>(GARDEN_LOADER_ID) == null) {
            loaderManager.initLoader(GARDEN_LOADER_ID, null, this)
        } else {
            loaderManager.restartLoader(GARDEN_LOADER_ID, null, this)
        }
    }
}