package fr.lyon1.m22.potager.parsing

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import fr.lyon1.m22.potager.demo.DummyObject
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Test
import java.io.IOException


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TestJsonParsing {

    @Test
    fun testJsonParsingFromString() {
        val test = DummyObject("test name", 12)

        val mapper = ObjectMapper()
        val testJson = "{\"name\": \"test name\", \"age\": \"12\"}"

        try {
            val parsedFromJson = mapper.readValue(testJson, DummyObject::class.java)
            assertEquals(test, parsedFromJson)
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    @Test
    fun testJsonParsingFromFile() {
        val test = DummyObject("test name", 12)
        val mapper = ObjectMapper()
        // file should be in src/test/resources
        val inputStream = javaClass.classLoader.getResourceAsStream("test.json")
        val parsedJsonStringFromFile = inputStream.bufferedReader().use { it.readText() }
        val parsedJsonFromFile = mapper.readValue(parsedJsonStringFromFile, DummyObject::class.java)
        assertEquals(test, parsedJsonFromFile)

    }

    @Test
    fun testJsonTreeParsingFromFile() {
        val test1 = DummyObject("test name", 12)
        val test2 = DummyObject("john doe", 18)
        val test3 = DummyObject("robert", 82)
        val test4 = DummyObject("pascal", 12)
        val test5 = DummyObject("emmy", 32)
        val testArray: Array<DummyObject> = arrayOf(test1, test2, test3, test4, test5)
        val mapper = ObjectMapper()
        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true)
        // file should be in src/test/resources
        val inputStream = javaClass.classLoader.getResourceAsStream("testArray.json")
        val parsedJsonStringFromFile = inputStream.bufferedReader().use { it.readText() }
        val parsedJsonFromFile = mapper.readValue(parsedJsonStringFromFile,
                Array<DummyObject>::class.java)
        assertArrayEquals(testArray, parsedJsonFromFile)
    }

}
