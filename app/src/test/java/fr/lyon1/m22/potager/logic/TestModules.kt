package fr.lyon1.m22.potager.logic

import fr.lyon1.m22.potager.data.model.Answer
import fr.lyon1.m22.potager.data.model.Module
import fr.lyon1.m22.potager.data.model.OpenQuestion
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class TestModules {

    @Before
    fun resetCounters() {
        Module.counter = 0
    }

    @Test
    fun testSubClasses() {

        var openQuestion = OpenQuestion(null,
                "name",
                "description",
                "question",
                10,
                Array(1, { _ -> Answer(Array(1, { _ -> "correct" })) }),
                5)

        Assert.assertEquals(1, openQuestion.id)

        openQuestion = OpenQuestion(1,
                "name",
                "description",
                "question",
                10,
                Array(1, { _ -> Answer(Array(1, { _ -> "correct" })) }),
                0)

        Assert.assertEquals(2, openQuestion.id)
    }
}