package fr.lyon1.m22.potager.logic

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import fr.lyon1.m22.potager.data.DataHandler
import fr.lyon1.m22.potager.data.model.Module
import fr.lyon1.m22.potager.profile.UserProfile
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TestProfile {

    @Before
    fun resetCounters() {
        Module.counter = 0
    }

    // TODO: test exam after it's implemented


    @Test
    fun testNextModule() {
        val mapper = ObjectMapper().registerKotlinModule()
        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true)
        // file should be in src/test/resources
        val inputStream = javaClass.classLoader.getResourceAsStream("sections.json")
        DataHandler.parseSections(inputStream)
        UserProfile.createNew(DataHandler.sections!!)
        assertEquals(1, UserProfile.nextModule())
        assertEquals(DataHandler.getModuleById(1)!!.description,
                "description of module 1 of subsection A.1")
    }

}

