package fr.lyon1.m22.potager.ui.learn

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.kofigyan.stateprogressbar.StateProgressBar
import fr.lyon1.m22.potager.R
import fr.lyon1.m22.potager.data.DataHandler
import fr.lyon1.m22.potager.data.model.Module
import fr.lyon1.m22.potager.logic.ModuleGrader
import fr.lyon1.m22.potager.profile.UserProfile
import fr.lyon1.m22.potager.ui.MainScreenActivity
import kotlinx.android.synthetic.main.state_progress_contianer.*
import java.lang.Math.min

class SectionActivity : AppCompatActivity() {

    var sectionId: Int = 0
        set(value) {
            UserProfile.lastVisitedSectionId = value
            field = value
        }

    companion object {

        private const val SECTION_ID = "section_id"
        private const val MODULE_ID = "module_id"

        fun newIntent(context: Context, moduleId: Int, sectionId: Int): Intent {
            val intent = Intent(context, SectionActivity::class.java)
            intent.putExtra(SECTION_ID, sectionId)
            intent.putExtra(MODULE_ID, moduleId)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.section_activity)
        title = DataHandler.sections?.get(intent.extras.getInt(SECTION_ID))?.name ?: "Section"
        sectionId = intent.extras.getInt(SECTION_ID)
        UserProfile.lastModuleVisitedId = intent.extras.getInt(MODULE_ID)
        loadModuleById(intent.extras.getInt(MODULE_ID))
        loadProgressBar(intent.extras.getInt(SECTION_ID))
    }

    private fun loadProgressBar(int: Int) {
        val sectionProgressBar = SectionProgressContainerFragment.newInstance(int)
        Log.d("fragment loading", "isnull?: ${sectionProgressBar.toString()}")
        supportFragmentManager.beginTransaction().replace(R.id.progressContainer,
                                                          sectionProgressBar).commit()
    }


    private var module: Module? = null
    private var lastObtainedScore: Int = 0

    private fun loadModuleById(id: Int?) {
        id?.let {
            val moduleFragment = ModuleContainerFragment.newInstance(it)
            moduleFragment.onAnswer = { onValidate(it) }
            supportFragmentManager.beginTransaction().replace(R.id.moduleContainer,
                                                              moduleFragment).commit()
            module = DataHandler.getModuleById(id)
            UserProfile.lastModuleVisitedId = it
        } ?: run {
            val intent = MainScreenActivity.newIntent(this)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            Toast.makeText(this,
                           resources.getString(R.string.done_all_modules),
                           Toast.LENGTH_SHORT).show()
        }
    }


    private fun onValidate(answerString: String) {
        Log.d(MainScreenActivity.SECTION, "your answer: ${answerString}")
        ModuleGrader.validateAnswer(answerString)?.let {
            UserProfile.onFinishedModule(it)
            lastObtainedScore = it
            onAnswer()
        } ?: run {
            Toast.makeText(this,
                           resources.getString(R.string.wrong_answer_toast),
                           Toast.LENGTH_SHORT).show()
        }
    }

    private fun onAnswer() {
        Toast.makeText(this,
                       "$lastObtainedScore / ${module?.maxScore ?: 0}",
                       Toast.LENGTH_SHORT).show()
        if (stateProgressBar.currentStateNumber == stateProgressBar.maxStateNumber) {
            Log.d("progress", "statenumber: ${stateProgressBar.currentStateNumber}")
            UserProfile.walkSectionProgresses {
                if (it.sectionId == sectionId) {
                    it.lastModuleVisited = null
                }
            }
            loadModuleById(null)
        } else {
            stateProgressBar.loadNext()
            loadModuleById(UserProfile.lastModuleVisitedId + 1)
            Log.d("progress", "last module: ${UserProfile.lastModuleVisitedId + 1}")
        }
    }

    private fun loadNextSection() {
        sectionId = UserProfile.nextSectionId()!!
        loadProgressBar(sectionId)
        title = DataHandler.sectionById(sectionId)?.name ?: "Section $sectionId"
        Log.d("progress", "sectionId: $sectionId, title: $title")
    }

}

private fun StateProgressBar.loadNext() {
    setCurrentStateNumber(when (min(currentStateNumber, maxStateNumber - 1)) {
                              1 -> StateProgressBar.StateNumber.TWO
                              2 -> StateProgressBar.StateNumber.THREE
                              3 -> StateProgressBar.StateNumber.FOUR
                              4 -> StateProgressBar.StateNumber.FIVE
                              else -> StateProgressBar.StateNumber.FIVE
                          })
}
